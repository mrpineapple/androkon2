package androkon.version2;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	String appHost = "http://ianfhunter.com/Androkon/"; 
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
    public void onResume(){
    	super.onResume();
        populateList();
    }
    public void populateList(){
    	ListView listview = (ListView) findViewById(R.id.main_layout);
    	
    	Database db = new Database(getApplicationContext());
    	ArrayList<Conference> cons = db.getLocallyStoredConventions();

    	ArrayList<String> constrings = new ArrayList<String>();
    	for(int i = 0; i != cons.size();i++){
    		constrings.add(cons.get(i).getName());
    	}
    	
    	String[] values = new String[] { "Example","Text" };
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
    	        android.R.layout.simple_list_item_1,
    	        constrings);
//    	for (int i = 0; i < values.length; ++i) {
//  	      adapter.add(values[i]);
//    	}

    	
    	listview.setAdapter(adapter);
    	listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		@Override
    	     public void onItemClick(AdapterView<?> parent, final View view,
    	          int position, long id) {
    			System.out.println("HI");
    		}
    	});
    	ImageButton b = (ImageButton)findViewById(R.id.downloadButton);
    	b.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				upDateDBClicked(v);
			}
    	});
    	
    	
    	
    }
	public void upDateDBClicked(View view)
	{
		String message;
		try {
			message = new PissOffNetworkingInMainThreadException().execute(this).get();

			Database db = new Database(this);

			db.open();
			Conference cons[] = null;
	        
	        // Don't spazz when new fields are added to the db
	        try
	        {
	        	cons = db.returnConference();
	        }
	        catch(Exception e)
	        {
	        	db.clear();
	        	cons = db.returnConference();
	        	//b.setText("Update Required");	//set icon to a download with an exclamation mark
	        }	        
	        db.close();
	        
	    	    
	        
	        
	    	ListView listview = (ListView) findViewById(R.id.main_layout);
	    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	    	        android.R.layout.simple_list_item_1,
	    	        new ArrayList<String>());
	        for(int i = 0; i < cons.length; i++) 
	      	    adapter.add(cons[i].getName());	        
	    	listview.setAdapter(adapter);
	        
		} catch (InterruptedException e) {
			message = "Operation Interrupted.";
		} catch (ExecutionException e) {
			message = "Could not execute, are you connected to the internet?";
		} catch (Exception e){
			message = "Unknown Error";
		}
		Toast.makeText(this.getApplicationContext(),message , Toast.LENGTH_SHORT).show();
		
		
		
/*		Intent intent = new Intent(this, UpdateDBActivity.class);
		startActivity(intent);*/
	}    
}

   /*
    public void createButtons(){
    	LinearLayout ll = (LinearLayout)findViewById(R.id.main_layout);
    	ll.removeAllViews();
    	
    	Button b = new Button(this);
		b.setText("Update Cons");
		b.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				upDateDBClicked(v);
			}
		});
		
		@SuppressWarnings("deprecation") // FILL_PARENT is deprecated since API level 8, but we're targeting 7
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		ll.addView(b, lp);
		
    	
    	Database db = new Database(this);
        db.open();
        
        Conference cons[] = null;
        
        // Don't spazz when new fields are added to the db
        try
        {
        	cons = db.returnConference();
        }
        catch(Exception e)
        {
        	db.clear();
        	cons = db.returnConference();
        	b.setText("Update Required");
        }
        
        db.close();
        
		for(int i = 0; i < cons.length; i++) 
			this.addConButton(cons[i]);	
    }
    
    private class ConOnClickListener implements View.OnClickListener{
    	public int con_id;
		
		@Override
		public void onClick(View v) {
			conClicked(v, con_id);	
		}
    }
    
    public void conClicked(View view, int id)
    {
    	Intent intent = new Intent(this, ViewConAct.class);
    	intent.putExtra("id", id);
    	startActivity(intent);
    }
    
    //add con selection button
    public void addConButton(Conference con)
    {
	    Button b = new Button(this);
		b.setText(con.getName());
		
		ConOnClickListener click = new ConOnClickListener();
		click.con_id = con.getId();
		
		b.setOnClickListener(click);
		
		LinearLayout ll = (LinearLayout)findViewById(R.id.main_layout);
		
		@SuppressWarnings("deprecation") // FILL_PARENT is deprecated since API level 8, but we're targeting 7
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		ll.addView(b, lp);
    }
    
    public void upDateDBClicked(View view)
    {
    	Intent intent = new Intent(this, UpdateDBActivity.class);
    	startActivity(intent);
    }    
}

*/