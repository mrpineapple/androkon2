package androkon.version2;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.Menu;


class PissOffNetworkingInMainThreadException extends AsyncTask<Context, Void, String> {
	String appHost = "http://ianfhunter.com/Androkon/"; 

	//fetching JSON Objects
	public JSONArray getJSON(String extension, String appHostToast) throws IOException, JSONException{
			URL url = new URL(appHostToast + "/json_" + extension);
			InputStream is = url.openStream();

			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
	
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();			
			return new JSONArray(sb.toString());
	}
		
	@Override
	protected String doInBackground(Context...context){
		Database db = null;	
		String text = null;			
		try {						
			JSONArray json_cons = getJSON("cons",appHost);
			JSONArray json_events = getJSON("events",appHost);
			JSONArray json_maps = getJSON("maps",appHost);

			//lol ok.
			for(Context c : context)
				db = new Database(c);
			
			db.open();
			db.clear();

			for (int i = 0; i < json_cons.length(); i++) {
				JSONObject con = json_cons.getJSONObject(i);
				int id = con.getInt("pk");
				con = con.getJSONObject("fields");

				db.createConferenceEntry(id, con.getString("name"), con.getString("description"),
						con.getString("start_date"), con.getString("end_date"), 
						con.getString("website"), con.getString("guests"), con.getString("twitter"), con.getString("gmaps"));
			}
			
			for (int i = 0; i < json_events.length(); i++) {
				JSONObject con = json_events.getJSONObject(i);
				int id = con.getInt("pk");
				con = con.getJSONObject("fields");

				db.createEventEntry(id, con.getString("name"), con.getString("time"), con.getString("end_time"),
						con.getString("location"), con.getString("description"), con.getInt("conference"));
			}
			
			File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/androkon/");
			
			// Clear out old files
			try{
				DeleteRecursive(dir);
			}
			catch (Exception e) {}
            dir.mkdirs();
            
			
			for (int i = 0; i < json_maps.length(); i++) {
				JSONObject con = json_maps.getJSONObject(i);
				int id = con.getInt("pk");
				con = con.getJSONObject("fields");
				
				String fileName = con.getString("picture");
							
				db.createMapEntry(id, fileName, con.getInt("con"));
				
//				URL url = new URL(appHost + "static/" + fileName);
//				URLConnection ucon = url.openConnection();
//				ucon.setReadTimeout(5000);
//				ucon.setConnectTimeout(30000);
//				
//				InputStream is = ucon.getInputStream();
//	            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
//	            
//	            
//	            FileOutputStream outStream = new FileOutputStream(
//	            		Environment.getExternalStorageDirectory().getPath() + "/androkon/" + fileName);
//	            byte[] buff = new byte[5 * 1024];
//
//	            int len;
//	            while ((len = inStream.read(buff)) != -1)
//	            {
//	                outStream.write(buff,0,len);
//	            }
//	            
//	            outStream.flush();
//	            outStream.close();
//	            inStream.close();

			}

			
			// Display new cons to user
			text = "Update Successful!";			

		} catch (Exception e) {;
			e.printStackTrace();
			text = "Something went wrong when connecting to the update server. \nAre you connected to the Internet?";
		}
		
		// Make sure we always close the db
		if(db != null)
			db.close();

		
        //saving a local copy of the DB to file
		db.open();
        
		Conference cons[] = db.returnConference();
		JSONObject toSave;
		for (int i = 0; i < cons.length; i++){
			try {
				toSave = new JSONObject();

				//convention
				toSave.put("id",cons[i].getId());
				toSave.put("name",cons[i].getName());
				toSave.put("twitter",cons[i].getTwitter());
				toSave.put("website",cons[i].getWebsite());
				toSave.put("description",cons[i].getDescription());
				toSave.put("GMaps",cons[i].getGmaps());
				toSave.put("start_date",cons[i].getStartString());					
				toSave.put("end_date",cons[i].getEndString());
				toSave.put("guests",cons[i].getGuests());
				
				String fileName = "ConventionFile" + cons[i].getId();
				FileOutputStream outStream = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/androkon/" + fileName);
				outStream.write(toSave.toString().getBytes());
				outStream.close();

				//associated events
				ConEvent events[] = db.getConEvents(cons[i].getId());
				for(int j = 0; j < events.length;j++){
					toSave = new JSONObject();
					toSave.put("Id", events[j].getId());
					toSave.put("Name", events[j].getName());
					toSave.put("Description", events[j].getDescription());
					toSave.put("End", events[j].getEndTimeString());
					toSave.put("Start", events[j].getTimeString());
					toSave.put("Location", events[j].getLocation());

					fileName = cons[i].getId() + "EventFile" + events[j].getId(); 
					outStream = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/androkon/" + fileName);
					outStream.write(toSave.toString().getBytes());
					outStream.close();
				
				}
				
				//associated maps
				ConMap maps[] = db.getConMaps(cons[i].getId());
				for(int k = 0; k < maps.length;k++){
					toSave = new JSONObject();
					toSave.put("Id", maps[k].getId());
					toSave.put("Path", maps[k].getPath());

					fileName = cons[i].getId() + "MapFile" + events[k].getId(); 
					outStream = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/androkon/" + fileName);
					outStream.write(toSave.toString().getBytes());
					outStream.close();
				
				}
				
			} catch (Exception e) {
				System.out.println("Something Went Wrong When Saving A Local Copy");
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return text;
	}
	
	private void DeleteRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	        {
	            child.delete();
	            DeleteRecursive(child);
	        }

	    fileOrDirectory.delete();
	}
}

