/*  
package androkon.version2;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ViewConAct extends ListActivity {
	
	private void setText(int id, String text)
	{
		TextView t = (TextView)findViewById(id);
		t.setText(text);
	}
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_con);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		int conId = getIntent().getExtras().getInt("id");
		
		Database db = new Database(this);
		db.open();
		
		ConEvent[] conEvents = db.getConEvents(conId);
		ConMap[] conMaps = db.getConMaps(conId);
				
		String text = db.getCon(conId) + "\nEvents:\n";
		
		db.close();
		
		
		for(int i = 0; i != conEvents.length; i++)
			text += conEvents[i];
		
		text += "\nMaps:\n";
		ArrayList<String> listItems=new ArrayList<String>();
		listItems.add(text);
		for(int i = 0; i != conMaps.length; i++)
//			text += conMaps[i];
			listItems.add(conMaps[i]+ "");

		ArrayAdapter<String> adapter;
		adapter=new ArrayAdapter<String>(this,
	            android.R.layout.simple_list_item_1,
	            listItems);
	        setListAdapter(adapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_test_act, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
 */